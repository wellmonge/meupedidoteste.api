import { request, expect } from '../helpers';
import { defaultOrder } from '../../utils/constants';

describe('Order Routes', () => {
  describe('Rout GET', () => {
    it('should GET all Orders', (done) => {
      request
        .get('/order/findall')
        .then((res) => {
          expect(res.body).to.be.an('array');
          done();
        });
    });

    it('should GET by ID Order', (done) => {
      request
        .get('/order/findbyid/')
        .then((res) => {
          // expect(res.body).to.be.an('array');
          done();
        });
    });

    it('should GET by FILTER Order', (done) => {
      request
        .get('/order/findby')
        .query({
          'productToOrder.product.name': 'A280 blaster rifle',
        })
        .then((res) => {
          // expect(res.body).to.be.an('array');
          done();
        });
    });
  });

  describe('Rout POST /order/create', () => {
    it('should CREATE a Order', (done) => {
      request
        .post('/order/create')
        .set('Content-Type', 'application/json')
        .send(defaultOrder)
        .then((res) => {
          expect(res.body.Data._id).to.be.equal(defaultOrder._id);
          done();
        });
    });
  });

  describe('Rout PUT /order/update', () => {
    it('should UPDATE a Order', (done) => {
      request
        .put('/order/update')
        .set('Content-Type', 'application/json')
        .send({
          _id: defaultOrder._id,
          productToOrder: [],
          client: null,
          totalByOrder: 200,
        })
        .then((res) => {
          expect(res.body.Data.totalByOrder).to.be.equal(200);
          done();
        });
    });
  });

  describe('Rout DELETE /order/remove', () => {
    it('should REMOVE a Order', (done) => {
      request
        .delete('/order/remove/')
        .query({ _id: defaultOrder._id })
        .then((res) => {
          expect(res.status).to.be.equal(204);
          done();
        });
    });
  });
});
