import { request, expect } from '../helpers';
import { defaultClient } from '../../utils/constants';

describe('Client Routes', () => {
  const clientUpdated = 'Client updated';

  describe('Rout GET /client/findall', () => {
    it('should GET all clients', (done) => {
      request
        .get('/client/findall')
        .then((res) => {
          expect(res.body).to.be.an('array');
          done();
        }).catch(done());
    });
  });

  describe('Rout POST /client/create', () => {
    it('should CREATE a client', (done) => {
      request
        .post('/client/create')
        .set('Content-Type', 'application/json')
        .send(defaultClient)
        .then((res) => {
          expect(res.body.Data.name).to.be.eql(defaultClient.name, "Name's client matched!");
          done();
        })
        .catch(done);
    });
  });

  describe('Rout PUT /client/update', () => {
    it('should UPDATE a client', (done) => {
      request
        .put('/client/update')
        .send({
          name: clientUpdated,
          oldName: defaultClient.name,
        })
        .then((res) => {
          expect(res.body.Data.name).to.be.equal(clientUpdated, "Name's client matched!");
          done();
        }).catch(done);
    });
  });

  describe('Rout DELETE /client/remove', () => {
    it('should REMOVE a client', (done) => {
      request
        .delete('/client/remove')
        .query({ name: clientUpdated })
        .then((res) => {
          expect(res.status).to.be.equal(204);
          done();
        }).catch(done);
    });
  });
});
