
import Order from '../models/order';
import { successResult, errorResult } from '../utils/constants';
import { read } from 'fs';

// const urlBase = "/api/Order";
const urlBase = '/order';

module.exports = (app) => {
  app.get(`${urlBase}/findall`, (req, res) => {
    Order.model.find({}, (err, result) => {
      if (err) { res.json(err); }
      res.json(result);
    });
  });

  app.get(`${urlBase}/findby`, (req, res) => {
    Order.model.find(req.query, (err, result) => {
      if (err) { res.json(err); }
      res.json(result);
    });
  });

  app.get(`${urlBase}/findbyid/:id`, (req, res) => {
    Order.model.findById(req.param.id, (err, result) => {
      if (err) { res.json(err); }
      res.json(result);
    });
  });

  app.post(`${urlBase}/create`, (req, res) => {
    if (!req.body) return;
    const dataItem = new Order.model(req.body);
    req.body.productToOrders.forEach(element => {
      dataItem.productToOrder.push(new Order.ProductToOrder(element));
    });
    dataItem.save((err, OrderResult) => {
      if (err) {
        res.json(errorResult(err.Message));
      }

      res.json(successResult(OrderResult));
    });
  });

  app.put(`${urlBase}/update`, (req, res) => {
    if (!req.body) return;

    let updating = {};
    updating = Object.assign(updating, req.body);

    Order.model.findByIdAndUpdate(
      { _id: req.body._id }
      , updating
      , {
        new: true,
      }
      , (err, OrderResult) => {
        if (err) 
          res.json(errorResult(err.Message));
    
        res.json(successResult(OrderResult));
      },
    );
  });

  app.delete(`${urlBase}/remove`, (req, res) => {
    if (!req.query) return;
    Order.model.findOneAndRemove(
      req.query
      , (err) => {
        if (err) { res.sendStatus(412); }

        res.sendStatus(204);
      },
    );
  });
};

