import express from 'express';
import load from 'express-load';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import errorHandler from 'error-handler';
import dotenv from 'dotenv';

const app = express();
dotenv.config();

// all environments
app.disable('x-powered-by');
app.set('port', process.env.PORT || 3000);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text());

app.use(bodyParser.json({ type: 'application/json' }));
app.use('/', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
app.use(methodOverride('_method'));
app.set('superSecret', process.env.SECRET);

const env = process.env.NODE_ENV || 'development';

// development only
if (env === 'development') {
  app.use(errorHandler);
}
// production only
if (env === 'production') {
  // TODO
}

// load modules
load('config')
  .then('utils')
  .then('routes')
  .into(app);

app.listen(app.get('port'), () => {
  console.log(`Express server listening on port ${app.get('port')}`);
});

export default app;
