import mongoose, { Schema } from 'mongoose';
import Client from './client';
import Product from './product';

const productToOrderSchema = new Schema({
  product: Product.Schema,
  unitPrice: { type: Number },
  quantity: { type: Number },
  totalByProduct: { type: Number },
});

const orderSchema =
  new Schema({
    productToOrder: [productToOrderSchema],
    client: Client.Schema,
    totalByOrder: { type: Number },
    createdAt: { type: Date, default: Date.now },
  });

export default {
  Schema: orderSchema,
  ProductToOrder: mongoose.model('productToOrder', productToOrderSchema),
  model: mongoose.model('order', orderSchema),
};
